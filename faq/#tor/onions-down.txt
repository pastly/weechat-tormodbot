Most onion sites are run by amateurs and are thus going down
temporarily/permanently all the time. This can be seemingly exacerbated by
indexes not removing links that have been down for a significant length of
time.  If you can access onion services on this list
(https://github.com/alecmuffett/real-world-onion-sites) that are marked as up,
then the other onion services you're trying to access are most likely down.
There's most likely nothing wrong with your configuration.

v2 onion services will stop working in July 2021 for most people. For why, ask
"!faq v2-onions".

V3 onion services were flaky in January 2021. For why
https://blog.pastly.net/posts/2021-01-13-tracking-tors-v3-onion-outages/

There's not really a good search engine that indexes onion services. Maybe try
http://ahmia.fi.

If you've never made a search engine before, please don't try to make one for
onion services. It won't be any better than anything that already exists.
Instead, maybe run a relay or fix a bug or host your blog as an onion service.
Get involved and be an advocate for Tor!  https://community.torproject.org

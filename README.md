# TorModBot

A Weechat Python (3) plugin to aide moderation of multiple IRC channels.

Documentation exists in `docs/` and as docstrings in code. It's rendered with
Sphinx and deployed "regularly" at <https://tormodbot.pastly.net>.
